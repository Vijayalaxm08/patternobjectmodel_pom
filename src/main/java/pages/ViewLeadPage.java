package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="viewLead_firstName_sp")
	private WebElement eleViewFirstName;
	public ViewLeadPage verifyFirstName() {
		verifyExactText(eleViewFirstName, "Vijayalaxmi");
		return this;
	}
	
	@FindBy(linkText="Edit")
	private WebElement eleEdit;
	public EditLeadPage clickEditButton() {
		click(eleEdit);
		return new EditLeadPage();
	}
	
	@FindBy(id="viewLead_companyName_sp")
	private WebElement eleCompanyNameChanged;
	public ViewLeadPage verifyCompanyName(String companyName) {
		verifyPartialText(eleCompanyNameChanged, companyName);
		return this;
	}
	
	public ViewLeadPage verifyPageTitle() {
		verifyTitle("View Lead | opentaps CRM");
		return this;
	}
	
	@FindBy(linkText="Duplicate Lead")
	private WebElement eleDuplicateLead;
	public DuplicateLeadPage clickDuplicateLeadButton() {
		click(eleDuplicateLead);
		return new DuplicateLeadPage();
	}
	
	@FindBy(linkText="Delete")
	private WebElement eleDelete;
	public LeadPage clickDeleteButton() {
		click(eleDelete);
		return new LeadPage();
	}
}





