package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="updateLeadForm_companyName")
	private WebElement eleCompanyName;
	public EditLeadPage editCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	
	@FindBy(xpath="(//input[@name='submitButton'])[1]")
	private WebElement eleUpdate;
	public ViewLeadPage clickUpdateButton() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
}





