package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods{
  
	@BeforeTest
	public void setData() {
		testCaseName="TC004_DeleteLead";
		testDescription="Editing the Lead";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC004_DeleteLead";
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String uname, String pwd, String phoneNumber) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfaLink()
		.clickLeads()
		.clickFindLeadsLink()
		.clickPhoneTab()
		.typePhoneNumber(phoneNumber)
		.clickFindLeadsButton()
		.getFirstLeadID()
		.clickFirstLeadID()
		.clickDeleteButton()
		.clickFindLeadsLink()
		.typeLeadID()
		.clickFindLeadsButton()
		.verifyErrorMessage();
					
	}
	
}
