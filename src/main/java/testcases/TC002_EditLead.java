package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods{
  
	@BeforeTest
	public void setData() {
		testCaseName="TC002_EditLead";
		testDescription="Editing the Lead";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="Tc002_EditLead";
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String uname, String pwd, String CompanyName) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfaLink()
		.clickLeads()
		.clickFindLeadsLink()
		.typeFNameFindLead()
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.verifyPageTitle()
		.clickEditButton()
		.editCompanyName(CompanyName)
		.clickUpdateButton()
		.verifyCompanyName(CompanyName);	
	
	}
	
}
